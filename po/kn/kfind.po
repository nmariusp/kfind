# translation of kfindpart.po to Kannada
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Shankar Prasad <svenkate@redhat.com>, 2009, 2010.
# Vasudev Kamath <kamathvasudev@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kfindpart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-19 01:56+0000\n"
"PO-Revision-Date: 2010-01-28 22:50+0530\n"
"Last-Translator: Shankar Prasad <svenkate@redhat.com>\n"
"Language-Team: kn-IN <>\n"
"Language: kn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.0\n"

#: kfinddlg.cpp:31
#, fuzzy, kde-format
#| msgid "Find Files/Folders"
msgctxt "@title:window"
msgid "Find Files/Folders"
msgstr "ಕಡತಗಳನ್ನು/ಕಡತಕೋಶಗಳನ್ನು ಹುಡುಕು"

#: kfinddlg.cpp:48 kfinddlg.cpp:197
#, kde-format
msgctxt "the application is currently idle, there is no active search"
msgid "Idle."
msgstr "ನಿಶ್ಚಲ."

#. i18n as below
#: kfinddlg.cpp:133
#, fuzzy, kde-format
#| msgid "one file found"
#| msgid_plural "%1 files found"
msgid "0 items found"
msgstr "ಒಂದು ಕಡತ ಕಂಡುಬಂದಿದೆ"

#: kfinddlg.cpp:172
#, kde-format
msgid "Searching..."
msgstr "ಹುಡುಕಲಾಗುತ್ತಿದೆ..."

#: kfinddlg.cpp:199
#, kde-format
msgid "Canceled."
msgstr "ರದ್ದುಗೊಳಿಸಲಾಗಿದೆ."

#: kfinddlg.cpp:201 kfinddlg.cpp:204 kfinddlg.cpp:207
#, kde-format
msgid "Error."
msgstr "ದೋಷ."

#: kfinddlg.cpp:202
#, kde-format
msgid "Please specify an absolute path in the \"Look in\" box."
msgstr "ದಯವಿಟ್ಟು \"ಇದರಲ್ಲಿ ನೋಡು\" ಚೌಕದಲ್ಲಿ ಸಂಪೂರ್ಣ ಮಾರ್ಗವನ್ನು ಸೂಚಿಸಿ."

#: kfinddlg.cpp:205
#, kde-format
msgid "Could not find the specified folder."
msgstr "ಸೂಚಿಸಲಾದ ಕಡತಕೋಶವನ್ನು ಹುಡುಕಲಾಗಿಲ್ಲ."

#: kfinddlg.cpp:228 kfinddlg.cpp:252
#, fuzzy, kde-format
#| msgid "one file found"
#| msgid_plural "%1 files found"
msgid "one item found"
msgid_plural "%1 items found"
msgstr[0] "ಒಂದು ಕಡತ ಕಂಡುಬಂದಿದೆ"
msgstr[1] "%1 ಕಡತಗಳು ಕಂಡುಬಂದಿದೆ"

#: kfindtreeview.cpp:45
msgid "Read-write"
msgstr "ಓದಲು-ಬರೆಯಲು"

#: kfindtreeview.cpp:46
msgid "Read-only"
msgstr "ಓದಲು-ಮಾತ್ರ"

#: kfindtreeview.cpp:47
msgid "Write-only"
msgstr "ಬರೆಯಲು-ಮಾತ್ರ"

#: kfindtreeview.cpp:48
msgid "Inaccessible"
msgstr "ನಿಲುಕಿಸಿಕೊಳ್ಳಲಾಗುತ್ತಿಲ್ಲ"

#: kfindtreeview.cpp:68
#, kde-format
msgctxt "file name column"
msgid "Name"
msgstr "ಹೆಸರು"

#: kfindtreeview.cpp:70
#, kde-format
msgctxt "name of the containing folder"
msgid "In Subfolder"
msgstr "ಉಪಕಡತಕೋಶದಲ್ಲಿ"

#: kfindtreeview.cpp:72
#, kde-format
msgctxt "file size column"
msgid "Size"
msgstr "ಗಾತ್ರ"

#: kfindtreeview.cpp:74
#, kde-format
msgctxt "modified date column"
msgid "Modified"
msgstr "ಮಾರ್ಪಡಿಸಲಾದ"

#: kfindtreeview.cpp:76
#, kde-format
msgctxt "file permissions column"
msgid "Permissions"
msgstr "ಅನುಮತಿಗಳು"

#: kfindtreeview.cpp:78
#, kde-format
msgctxt "first matching line of the query string in this file"
msgid "First Matching Line"
msgstr "ತಾಳೆಯಾಗುವ ಮೊದಲು ಸಾಲು"

#: kfindtreeview.cpp:361
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr ""

#: kfindtreeview.cpp:362
#, kde-format
msgctxt "@info:whatsthis copy_location"
msgid "This will copy the path of the first selected item into the clipboard."
msgstr ""

#: kfindtreeview.cpp:367
#, kde-format
msgid "&Open containing folder(s)"
msgstr "ಇದನ್ನು ಹೊಂದಿರುವ ಕಡತಕೋಶವನ್ನು ತೆರೆ (&O)"

#: kfindtreeview.cpp:371
#, kde-format
msgid "&Delete"
msgstr "ಅಳಿಸಿಹಾಕು(&D)"

#: kfindtreeview.cpp:375
#, kde-format
msgid "&Move to Trash"
msgstr "ಕಸದ ಬುಟ್ಟಿಗೆ ಸ್ಥಳಾಂತರಿಸು(&M)"

#: kfindtreeview.cpp:515
#, fuzzy, kde-format
#| msgid "Save Results As"
msgctxt "@title:window"
msgid "Save Results As"
msgstr "ಫಲಿತಾಂಶಗನ್ನು ಹೀಗೆ ಉಳಿಸಿ"

#: kfindtreeview.cpp:540
#, kde-format
msgid "Unable to save results."
msgstr "ಫಲಿತಾಂಶಗಳನ್ನು ಉಳಿಸಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ."

#: kfindtreeview.cpp:554
#, kde-format
msgid "KFind Results File"
msgstr "KFind ಫಲಿತಾಂಶ ಕಡತ"

#: kfindtreeview.cpp:569
#, fuzzy, kde-format
#| msgid "Results were saved to file\n"
msgctxt "%1=filename"
msgid "Results were saved to: %1"
msgstr "ಫಲಿತಾಂಶಗಳನ್ನು ಕಡತಕ್ಕೆ ಉಳಿಸಲಾಗಿದೆ\n"

#: kfindtreeview.cpp:665 kftabdlg.cpp:396
#, kde-format
msgid "&Properties"
msgstr "ಗುಣಗಳು(&P)"

#: kftabdlg.cpp:66
#, kde-format
msgctxt "this is the label for the name textfield"
msgid "&Named:"
msgstr "ಹೀಗೆ ಹೆಸರಿಸಲಾದ(&N):"

#: kftabdlg.cpp:69
#, kde-format
msgid "You can use wildcard matching and \";\" for separating multiple names"
msgstr "ಹೆಸರುಗಳನ್ನು ಪ್ರತ್ಯೇಕಿಸಲು ವೈಲ್ಡ್‍ಕಾರ್ಡ್ ತಾಳೆ ಮಾಡುವಿಕೆ ಹಾಗು \";\" ಅನ್ನು ಬಳಸಬಹುದು"

#: kftabdlg.cpp:75
#, kde-format
msgid "Look &in:"
msgstr "ಇದರಲ್ಲಿ ನೋಡು(&i):"

#: kftabdlg.cpp:78
#, kde-format
msgid "Include &subfolders"
msgstr "ಉಪಕೋಶಗಳನ್ನು ಒಳಗೊಳ್ಳಿಸು(&s)"

#: kftabdlg.cpp:79
#, kde-format
msgid "Case s&ensitive search"
msgstr "ಕೇಸ್ ಸಂವೇದಿ ಹುಡುಕಾಟ (&e)"

#: kftabdlg.cpp:80
#, kde-format
msgid "&Browse..."
msgstr "ವೀಕ್ಷಿಸು(&B)..."

#: kftabdlg.cpp:81
#, kde-format
msgid "&Use files index"
msgstr "ಕಡತಗಳ ಸೂಚಿಯನ್ನು ಬಳಸು(&U)"

#: kftabdlg.cpp:82
#, kde-format
msgid "Show &hidden files"
msgstr "ಅಡಗಿಸಲಾದ ಕಡತಗಳನ್ನು ತೋರಿಸು(&h)"

#: kftabdlg.cpp:101
#, kde-format
msgid ""
"<qt>Enter the filename you are looking for. <br />Alternatives may be "
"separated by a semicolon \";\".<br /><br />The filename may contain the "
"following special characters:<ul><li><b>?</b> matches any single character</"
"li><li><b>*</b> matches zero or more of any characters</li><li><b>[...]</b> "
"matches any of the characters between the braces</li></ul><br />Example "
"searches:<ul><li><b>*.kwd;*.txt</b> finds all files ending with .kwd or ."
"txt</li><li><b>go[dt]</b> finds god and got</li><li><b>Hel?o</b> finds all "
"files that start with \"Hel\" and end with \"o\", having one character in "
"between</li><li><b>My Document.kwd</b> finds a file of exactly that name</"
"li></ul></qt>"
msgstr ""

#: kftabdlg.cpp:122
#, kde-format
msgid ""
"<qt>This lets you use the files' index created by the <i>slocate</i> package "
"to speed-up the search; remember to update the index from time to time "
"(using <i>updatedb</i>).</qt>"
msgstr ""

#: kftabdlg.cpp:166
#, kde-format
msgid "Find all files created or &modified:"
msgstr "ರಚಿಸಲಾದ ಅಥವ ಮಾರ್ಪಡಿಸಲಾದ ಎಲ್ಲಾ ಕಡತಗಳನ್ನು ಹುಡುಕು(&m):"

#: kftabdlg.cpp:168
#, kde-format
msgid "&between"
msgstr "ನಡುವೆ(&b)"

#: kftabdlg.cpp:170
#, kde-format
msgid "and"
msgstr "ಹಾಗು"

#: kftabdlg.cpp:192
#, kde-format
msgid "File &size is:"
msgstr "ಕಡತದ ಗಾತ್ರ(&s):"

#: kftabdlg.cpp:205
#, kde-format
msgid "Files owned by &user:"
msgstr "ಈ ಬಳಕೆದಾರನಿಂದ ಮಾಲಿಕತ್ವವನ್ನು ಹೊಂದಿದೆ(&u):"

#: kftabdlg.cpp:210
#, kde-format
msgid "Owned by &group:"
msgstr "ಈ ಗುಂಪಿನಿಂದ ಮಾಲಿಕತ್ವವನ್ನು ಹೊಂದಿದೆ(&g):"

#: kftabdlg.cpp:213
#, kde-format
msgctxt "file size isn't considered in the search"
msgid "(none)"
msgstr "(ಯಾವುದೂ ಇಲ್ಲ}"

#: kftabdlg.cpp:214
#, kde-format
msgid "At Least"
msgstr "ಕಡೆಪಕ್ಷ"

#: kftabdlg.cpp:215
#, kde-format
msgid "At Most"
msgstr "ಇದಕ್ಕೆ ಸಮನಾದ"

#: kftabdlg.cpp:216
#, kde-format
msgid "Equal To"
msgstr "ಇದಕ್ಕೆ ಸಮನಾದ"

#: kftabdlg.cpp:218 kftabdlg.cpp:829
#, kde-format
msgid "Byte"
msgid_plural "Bytes"
msgstr[0] "ಬೈಟ್‌"
msgstr[1] "ಬೈಟ್‌ಗಳು"

#: kftabdlg.cpp:219
#, kde-format
msgid "KiB"
msgstr "KiB"

#: kftabdlg.cpp:220
#, kde-format
msgid "MiB"
msgstr "MiB"

#: kftabdlg.cpp:221
#, kde-format
msgid "GiB"
msgstr "GiB"

#: kftabdlg.cpp:284
#, kde-format
msgctxt "label for the file type combobox"
msgid "File &type:"
msgstr "ಕಡತದ ಬಗೆ(&t):"

#: kftabdlg.cpp:289
#, kde-format
msgid "C&ontaining text:"
msgstr "ಹೊಂದಿರುವ ಪಠ್ಯ(&o):"

#: kftabdlg.cpp:295
#, kde-format
msgid ""
"<qt>If specified, only files that contain this text are found. Note that not "
"all file types from the list above are supported. Please refer to the "
"documentation for a list of supported file types.</qt>"
msgstr ""
"<qt>ಸೂಚಿಸಲಾಗಿದ್ದಲ್ಲಿ, ಪಠ್ಯವನ್ನು ಹೊಂದಿರುವ ಕಡತಗಳನ್ನು ಮಾತ್ರವೆ ಕಂಡು ಬರುತ್ತವೆ. ಮೇಲಿನ "
"ಪಟ್ಟಿಯಲ್ಲಿರುವ ಎಲ್ಲಾ ಕಡತದ ಬಗೆಗಳೂ ಬೆಂಬಲಿತವಾಗಿಲ್ಲ ಎಂದು ನೆನಪಿಡಿ. ಬೆಂಬಲವಿರುವ ಕಡತದ "
"ಬಗೆಗಳ ಪಟ್ಟಿಗಾಗಿ ದಯವಿಟ್ಟು ದಸ್ತಾವೇಜನ್ನು ನೋಡಿ.</qt>"

#: kftabdlg.cpp:303
#, kde-format
msgid "Case s&ensitive"
msgstr "ಕೇಸ್ ಸಂವೇದಿ(&e)"

#: kftabdlg.cpp:304
#, kde-format
msgid "Include &binary files"
msgstr "ಬೈನರಿ ಕಡತಗಳನ್ನೂ ಸೇರಿಸು(&b)"

#: kftabdlg.cpp:307
#, kde-format
msgid ""
"<qt>This lets you search in any type of file, even those that usually do not "
"contain text (for example program files and images).</qt>"
msgstr ""
"<qt>ಇದು ಪಠ್ಯಗಳಿರದ ಕಡತಗಳನ್ನೂ ಸೇರಿ (ಉದಾಹರಣೆಗೆ ಪ್ರೊಗ್ರಾಮ್ ಕಡತಗಳು ಹಾಗು ಚಿತ್ರಗಳು) "
"ಯಾವುದೆ ಬಗೆಯ ಕಡತದಲ್ಲಿಯಾದರೂ ಹುಡುಕಲು ಇದು ಅನುವು ಮಾಡಿಕೊಡುತ್ತದೆ.</qt>"

#: kftabdlg.cpp:314
#, fuzzy, kde-format
#| msgid "fo&r:"
msgctxt "as in search for"
msgid "fo&r:"
msgstr "ಇದಕ್ಕಾಗಿ(&r):"

#: kftabdlg.cpp:316
#, kde-format
msgid "Search &metainfo sections:"
msgstr "ಮೆಟಾಮಾಹಿತಿ ವಿಭಾಗಗಳನ್ನು ಹುಡುಕು(&m):"

#: kftabdlg.cpp:320
#, kde-format
msgid "All Files & Folders"
msgstr "ಎಲ್ಲಾ ಕಡತಗಳು ಹಾಗು ಕಡತಕೋಶಗಳು"

#: kftabdlg.cpp:321
#, kde-format
msgid "Files"
msgstr "ಕಡತಗಳು"

#: kftabdlg.cpp:322
#, kde-format
msgid "Folders"
msgstr "ಕಡತಕೋಶಗಳು"

#: kftabdlg.cpp:323
#, kde-format
msgid "Symbolic Links"
msgstr "ಸಾಂಕೇತಿಕ ಕೊಂಡಿಗಳು"

#: kftabdlg.cpp:324
#, kde-format
msgid "Special Files (Sockets, Device Files, ...)"
msgstr "ವಿಶೇಷ ಕಡತಗಳು (ಸಾಕೆಟ್‌ಗಳು, ಸಾಧನ ಕಡತಗಳು,...)"

#: kftabdlg.cpp:325
#, kde-format
msgid "Executable Files"
msgstr "ಕಾರ್ಯಗತಗೊಳಿಸಬಲ್ಲ ಕಡತಗಳು"

#: kftabdlg.cpp:326
#, kde-format
msgid "SUID Executable Files"
msgstr "ಕಾರ್ಯಗತಗೊಳಿಸಬಲ್ಲ SUID ಕಡತಗಳು"

#: kftabdlg.cpp:327
#, kde-format
msgid "All Images"
msgstr "ಎಲ್ಲಾ ಚಿತ್ರಗಳು"

#: kftabdlg.cpp:328
#, kde-format
msgid "All Video"
msgstr "ಎಲ್ಲಾ ವೀಡಿಯೋಗಳು"

#: kftabdlg.cpp:329
#, kde-format
msgid "All Sounds"
msgstr "ಎಲ್ಲಾ ಧ್ವನಿಗಳು"

#: kftabdlg.cpp:394
#, kde-format
msgid "Name/&Location"
msgstr "ಹೆಸರು/ತಾಣ(&L)"

#: kftabdlg.cpp:395
#, kde-format
msgctxt "tab name: search by contents"
msgid "C&ontents"
msgstr "ವಿಷಯಗಳು(&o)"

#: kftabdlg.cpp:400
#, kde-format
msgid ""
"<qt>Search within files' specific comments/metainfo<br />These are some "
"examples:<br /><ul><li><b>Audio files (mp3...)</b> Search in id3 tag for a "
"title, an album</li><li><b>Images (png...)</b> Search images with a special "
"resolution, comment...</li></ul></qt>"
msgstr ""

#: kftabdlg.cpp:408
#, kde-format
msgid ""
"<qt>If specified, search only in this field<br /><ul><li><b>Audio files "
"(mp3...)</b> This can be Title, Album...</li><li><b>Images (png...)</b> "
"Search only in Resolution, Bitdepth...</li></ul></qt>"
msgstr ""
"<qt>ಸೂಚಿಸಲಾಗಿದ್ದಲ್ಲಿ, ಕೇವಲ ಈ ಕ್ಷೇತ್ರದಲ್ಲಿ ಮಾತ್ರವೆ ಹುಡುಕು<br /><ul><li><b>ಆಡಿಯೋ "
"ಕಡತಗಳು (mp3...)</b> ಇದು ಶೀರ್ಷಿಕೆಯಾದಲ್ಲಿ, ಆಲ್ಬಮ್...</li><li><b>ಚಿತ್ರಗಳು (png...)</"
"b> ಕೇವಲ ರೆಸಲ್ಯೂಶನ್, ಬಿಟ್‌ಡೆಪ್ತ್‍ನಲ್ಲಿ ಮಾತ್ರವೆ ಹುಡುಕು...</li></ul></qt>"

#: kftabdlg.cpp:549
#, kde-format
msgid "Unable to search within a period which is less than a minute."
msgstr "ಒಂದು ನಿಮಿಷಕ್ಕಿಂತ ಕಡಿಮೆ ಸಮಯದ ಅವಧಿಯಲ್ಲಿ ಹುಡುಕಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ."

#: kftabdlg.cpp:560
#, kde-format
msgid "The date is not valid."
msgstr "ದಿನಾಂಕವು ಮಾನ್ಯವಾದುದಲ್ಲ."

#: kftabdlg.cpp:562
#, kde-format
msgid "Invalid date range."
msgstr "ದತ್ತಾಂಶ ವ್ಯಾಪ್ತಿಯು ಅಮಾನ್ಯವಾಗಿದೆ."

#: kftabdlg.cpp:564
#, kde-format
msgid "Unable to search dates in the future."
msgstr "ಭವಿಷ್ಯದ ದಿನಾಂಕಗಳನ್ನು ಹುಡುಕಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ."

#: kftabdlg.cpp:636
#, kde-format
msgid "Size is too big. Set maximum size value?"
msgstr "ಗಾತ್ರವು ಬಹಳ ದೊಡ್ಡದಾಗಿದೆ. ಗರಿಷ್ಟ ಗಾತ್ರದ ಮೌಲ್ಯಕ್ಕೆ ಹೊಂದಿಸಬೇಕೆ?"

#: kftabdlg.cpp:636
#, kde-format
msgid "Error"
msgstr "ದೋಷ"

#: kftabdlg.cpp:636
#, kde-format
msgid "Set"
msgstr "ಹೊಂದಿಸು"

#: kftabdlg.cpp:636
#, kde-format
msgid "Do Not Set"
msgstr "ಹೊಂದಿಸಬೇಡ"

#: kftabdlg.cpp:819
#, kde-format
msgctxt ""
"during the previous minute(s)/hour(s)/...; dynamic context 'type': 'i' "
"minutes, 'h' hours, 'd' days, 'm' months, 'y' years"
msgid "&during the previous"
msgid_plural "&during the previous"
msgstr[0] "ಹಿಂದೆ ನಡೆದ ಸಮಯದಲ್ಲಿ(&d)"
msgstr[1] "ಹಿಂದೆ ನಡೆದ ಸಮಯದಲ್ಲಿ(&d)"

#: kftabdlg.cpp:820
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "ನಿಮಿಷ"
msgstr[1] "ನಿಮಿಷಗಳು"

#: kftabdlg.cpp:821
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "hour"
msgid_plural "hours"
msgstr[0] "ಗಂಟೆ"
msgstr[1] "ಗಂಟೆಗಳು"

#: kftabdlg.cpp:822
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "day"
msgid_plural "days"
msgstr[0] "ದಿನ"
msgstr[1] "ದಿನಗಳು"

#: kftabdlg.cpp:823
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "month"
msgid_plural "months"
msgstr[0] "ತಿಂಗಳು"
msgstr[1] "ತಿಂಗಳುಗಳು"

#: kftabdlg.cpp:824
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "year"
msgid_plural "years"
msgstr[0] "ವರ್ಷ"
msgstr[1] "ವರ್ಷಗಳು"

#: kquery.cpp:556
#, fuzzy, kde-format
#| msgid "Error while using locate"
msgctxt "@title:window"
msgid "Error while using locate"
msgstr "ಪತ್ತೆಮಾಡುವುದನ್ನು ಬಳಸುವಾಗ ದೋಷ"

#: main.cpp:26
#, kde-format
msgid "KFind"
msgstr "KFind"

#: main.cpp:27
#, kde-format
msgid "KDE file find utility"
msgstr "KDE ಕಡತ ಹುಡುಕು ಸವಲತ್ತು"

#: main.cpp:28
#, fuzzy, kde-format
#| msgid "(c) 1998-2003, The KDE Developers"
msgid "(c) 1998-2021, The KDE Developers"
msgstr "(c) 1998-2003, KDE ವಿಕಸನಗಾರರು"

#: main.cpp:30
#, kde-format
msgid "Kai Uwe Broulik"
msgstr ""

#: main.cpp:30
#, kde-format
msgid "Current Maintainer"
msgstr "ಪ್ರಸ್ತುತ ಪಾಲಕ"

#: main.cpp:31
#, kde-format
msgid "Eric Coquelle"
msgstr "Eric Coquelle"

#: main.cpp:31
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgid "Former Maintainer"
msgstr "ಪ್ರಸ್ತುತ ಪಾಲಕ"

#: main.cpp:32
#, kde-format
msgid "Mark W. Webb"
msgstr "Mark W. Webb"

#: main.cpp:32
#, kde-format
msgid "Developer"
msgstr "ವಿಕಾಸಕರು"

#: main.cpp:33
#, kde-format
msgid "Beppe Grimaldi"
msgstr "Beppe Grimaldi"

#: main.cpp:33
#, kde-format
msgid "UI Design & more search options"
msgstr "UI ವಿನ್ಯಾಸ ಹಾಗು ಇತರೆ ಹುಡುಕು ಆಯ್ಕೆಗಳು"

#: main.cpp:34
#, kde-format
msgid "Martin Hartig"
msgstr "Martin Hartig"

#: main.cpp:35
#, kde-format
msgid "Stephan Kulow"
msgstr "Stephan Kulow"

#: main.cpp:36
#, kde-format
msgid "Mario Weilguni"
msgstr "Mario Weilguni"

#: main.cpp:37
#, kde-format
msgid "Alex Zepeda"
msgstr "Alex Zepeda"

#: main.cpp:38
#, kde-format
msgid "Miroslav Flídr"
msgstr "Miroslav Flídr"

#: main.cpp:39
#, kde-format
msgid "Harri Porten"
msgstr "Harri Porten"

#: main.cpp:40
#, kde-format
msgid "Dima Rogozin"
msgstr "Dima Rogozin"

#: main.cpp:41
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:42
#, kde-format
msgid "Hans Petter Bieker"
msgstr "Hans Petter Bieker"

#: main.cpp:43
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:43
#, kde-format
msgid "UI Design"
msgstr "UI ವಿನ್ಯಾಸ"

#: main.cpp:44
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:45
#, kde-format
msgid "Clarence Dang"
msgstr "Clarence Dang"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ಶಂಕರ ಪ್ರಸಾದ್ ಎಂ ವಿ"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "svenkate@redhat.com"

#: main.cpp:49
#, kde-format
msgid "Path(s) to search"
msgstr "ಹುಡುಕಬೇಕಿರುವ ಮಾರ್ಗ(ಗಳು)"

#~ msgid "Regular e&xpression"
#~ msgstr "ಸಾಮಾನ್ಯ ಎಕ್ಸ್‍ಪ್ರೆಶನ್(&x)"

#~ msgid "&Edit..."
#~ msgstr "ಸಂಪಾದಿಸು(&E)..."

#~ msgid "HTML page"
#~ msgstr "HTML ಪುಟ"

#~ msgid "Text file"
#~ msgstr "ಪಠ್ಯ ಕಡತ"

#~ msgctxt "Name of the component that finds things"
#~ msgid "Find Component"
#~ msgstr "ಹುಡುಕು ಅಂಗ"

#~ msgid "Aborted."
#~ msgstr "ಕಾರ್ಯಭಂಗಮಾಡಲಾಯಿತು (ಅಬಾರ್ಟ್)."

#~ msgid "Ready."
#~ msgstr "ಸಿದ್ಧವಿದೆ."

#~ msgid "Do you really want to delete the selected file?"
#~ msgid_plural "Do you really want to delete the %1 selected files?"
#~ msgstr[0] "ನೀವು ಆಯ್ಕೆ ಮಾಡಿದ ಕಡತವನ್ನು ನಿಜವಾಗಲು ಅಳಿಸಬೇಕೆ?"
#~ msgstr[1] "ನೀವು ಆಯ್ಕೆ ಮಾಡಿದ %1 ಕಡತಗಳನ್ನು ನಿಜವಾಗಲು ಅಳಿಸಬೇಕೆ?"

#~ msgctxt "Menu item"
#~ msgid "Open"
#~ msgstr "ತೆರೆಯಿರಿ"

#~ msgid "Open Folder"
#~ msgstr "ಕಡತಕೋಶವನ್ನು ತೆರೆಯಿರಿ"

#~ msgid "Copy"
#~ msgstr "ನಕಲಿಸಿ"

#~ msgid "Open With..."
#~ msgstr "ಇದರೊಂದಿಗೆ ತೆರೆಯಿರಿ..."

#~ msgid "Properties"
#~ msgstr "ಲಕ್ಷಣಗಳು"

#~ msgid "Selected Files"
#~ msgstr "ಆಯ್ಕೆ ಮಾಡಲಾದ ಕಡತಗಳು"
